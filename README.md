# Sistema de control de acceso a la Facultad de Ingenieria de la UAEMex

### Objetivo de proyecto

El objetivo de este proyecto es desarrollar un sistema para controlar el acceso a la Facultad de Ingeniería y comprobar que la persona que ingresa ha pasado por alguno de los filtros de sanitización. 

El personal que estará a cargo de cada filtro de sanitización dará de alta dicho filtro en un sistema web habilitado para tal propósito y a través de este sistema podrá generar un código QR, ya sea de forma digital, o en un documento imprimible, para colocarlo donde el filtro de sanitización este ubicado. 

Para las personas que ingresen a la facultad, se dispone de una aplicación móvil que deberán descargar en sus teléfonos y a través de la cual podrán escanear los códigos QR colocados en cada punto de sanitización una vez que pasen por ellos. Con esta acción la aplicación validará el código leído y le generará al usuario una notificación con información única que indicará que el usuario puede ingresar al edificio. 

Cuando el usuario se disponga a ingresar a un departamento de la facultad, por ejemplo, control escolar, biblioteca, etc., podrá mostrar como comprobante la notificación que le ha generado la aplicación, con la cual comprueba que verdaderamente, la persona pasó por el filtro de sanitización. 

Por otro lado, si la persona que desea ingresar a la facultad no cuenta con un dispositivo móvil inteligente con el cual pueda leer el código QR, se le otorgará el acceso leyendo el código de barras de su credencial, estos datos se guardaran en sistema para posteriormente cuando desee ingresar a otro espacio de la facultad, con el número de su cuenta se pueda verificar que realmente paso por el punto de sanitización.  

Si la persona no cuenta con ninguna de las opciones antes mencionadas, se contará con un sistema web en el cual deberá registrar sus datos para poder ingresar.

### Screenshots

![Pantalla principal de la app](https://gitlab.com/FDanielGomez/sistemacontrolcovid/-/raw/master/screenshots/mainscreen.png)

![Pantalla de scanner de QR de la app](https://gitlab.com/FDanielGomez/sistemacontrolcovid/-/raw/master/screenshots/camerascreen.png)

![Pantalla de status de la app](https://gitlab.com/FDanielGomez/sistemacontrolcovid/-/raw/master/screenshots/statusscreen.png)



