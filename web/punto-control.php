<?php
  include('database.php');
  session_start();
  if(!isset($_SESSION['rol']))
    {
        header('Location:login.php');
    }
    else{
        if(isset($_GET['id_cp']))
        {
            $records = $connection->prepare('SELECT * FROM control_points WHERE id_control_point = :id_cp');
            $records->bindParam('id_cp',$_GET['id_cp']);
            $records->execute();
            $cp = $records->fetch(PDO::FETCH_ASSOC);
            if(empty($cp))
            {
                die('Query failed');
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistema control covid</title>
    <!-- Boostrap-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/litera/bootstrap.min.css"></link>
</head>
<body>
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="">
          <img src="img/uaemex_logo.png" alt="UAEMex">
          UAEMex
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarColor03">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Caracteristicas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Acerca de</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?=$_SESSION['username']?></a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Configuracion de la cuenta</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="logout.php">Cerrar sesion</a>
              </div>
            </li>
          </ul>
          <ul class="navbar-nav ml-auto">
          </ul>
        </div>
    </nav>
    <!--Navbar-->

    <!--MAIN CONTAINER-->
    <div class="container">
        <div class="row">
          <div class="col-md-8 mt-4">
            <p class="lead">Punto de control:</p>
            <div class="row d-flex flex-column mt-4 ml-3">
                <h3 class=""><?=$cp['descripcion']?></h3>
                <h6 class="mt-4">Clave:</h6>
                <p id="id_cp"><?=$cp['id_control_point']?></p>
                <h6>Facultad:</h6>
                <p><?=$cp['facultad']?></p>
            </div>
          </div>
          <div class="col-md-4 mt-4">
            <h3>Codigo QR</h3>
            <div class="card mt-4">
              <div class="card-body">
                <div class="d-flex justify-content-center" id="qr_container">
                    <div class="spinner-grow text-success" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
              </div>
            </div>
            <a href ="" class="btn btn-success mt-4" id="download_button">DESCARGAR</a>
            <a href ="" class="btn btn-success mt-4" id="download_button">VOLVER A GENERAR</a>
          </div>
        </div>
      </div>
      <!--MAIN CONTAINER-->

    <!--scripts-->
    <!--<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>-->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="js/app.js"></script>
</body>
</html>