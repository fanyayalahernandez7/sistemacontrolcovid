<?php
    include ('vendor/autoload.php');
    use Endroid\QrCode\QrCode;
    date_default_timezone_set('America/Tegucigalpa');
    $id_cp = $_POST['id_cp'];
    $date_time = date('Y-m-d_H:i:s');
    $text = $id_cp.'_'.$date_time;
    $qrCode = new QrCode($text);
    $qrCode->setSize(300);
    $qrCode->writeFile(__DIR__.'/qrcodes/'.$id_cp.'.png');
    $image = $qrCode->writeString();
    $imageData = base64_encode($image);
    echo $imageData;
?>