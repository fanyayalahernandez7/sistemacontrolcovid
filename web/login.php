<?php
    include('database.php');
    session_start();

    $message = '';
    if (!empty($_POST['user_email']) && !empty($_POST['user_password'])) {
        $records = $connection->prepare('SELECT * FROM persona WHERE email = :user_email');
        $records->bindParam('user_email',$_POST['user_email']);
        $records->execute();
        $result_user = $records->fetch(PDO::FETCH_ASSOC);
        if (!empty($result_user)) { // && count($result_user > 0 )
            if ($result_user['password'] == $_POST['user_password']) {
                $_SESSION['user_id'] = $result_user['id_persona'];
                $_SESSION['user_email'] = $result_user['email'];
                $_SESSION['username'] = $result_user['nombre'];
                switch ($result_user['rol_code']) {
                    case 1:
                        $_SESSION['rol_code'] = $result_user['rol_code'];
                        $_SESSION['rol'] = $result_user['rol'];
                        header('Location:cp-list.php');
                        break;
                    default:
                    header('Location:login.php');
                        break;
                }
            }
            else {
                $message = 'Email o contrasena invalidos';
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Control de acceso FI UAEMex</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/litera/bootstrap.min.css"></link>


</head>
<body>
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="">
          <img src="img/uaemex_logo.png" alt="UAEMex">
          UAEMex
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarColor03">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Features</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Pricing</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">About</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <a class="dropdown-item" href="#">Something else here</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Separated link</a>
              </div>
            </li>
          </ul>
          <ul class="navbar-nav ml-auto">
          </ul>
        </div>
    </nav>
    <!--Navbar-->

    <!-- MAIN CONTAINER -->
    <div class="container">
        <div class="row">
            <div class="col-md-6 mt-4 mr-auto">
              <h1>Bienvenido al sistema de control de acceso de la Facultad de Ingenieria</h1>
                <img src="img/wearing_a_mask_.svg"  height="400" width="400" alt="Covid img">
            </div>
            <div class="col-md-4 mt-4 ml-auto">
                <div class="card">
                    <div class="card-body">
                        <h3 class="mb-4">Inicio de sesión</h3>

                        <?php if (!empty($message)) : ?>
                        <div class="alert alert-dismissible alert-danger">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <p><?=$message?></p>
                        </div>
                        <?php 
                        $message = '';
                        endif; ?>
                        <form action="login.php" method="POST">
                            <div class="form-group">
                                <label for="user_email">Correo institucional</label>
                                <input type="email" class="form-control" id="user_email" name="user_email" aria-describedby="emailHelp" placeholder="Ingresa tu correo institucional">
                            </div>
                            <div class="form-group">
                                <label for="user_password">Contraseña</label>
                                <input type="password" class="form-control" id="user_password" name="user_password" placeholder="Ingresa tu contrasena">
                            </div>
                            <input class="btn btn-success mx-auto" type="submit" value="INGRESAR" id="send_button">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>    
    <script src="js/app.js"></script>
</body>
</html>